package giovanni.services.interfaces;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.POST;

public interface PositionServices {
    @GET("v2/networks/ecobici")
    Call<Network> getResponses();
}

package giovanni.app.examenendcom.controller.ui.activitys;

import androidx.fragment.app.FragmentActivity;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;

import giovanni.app.examenendcom.R;
import giovanni.services.interfaces.PuntosResponse;
import giovanni.services.interfaces.Station;

public class MapsActivity extends FragmentActivity implements OnMapReadyCallback, GoogleMap.OnMarkerClickListener {

    private GoogleMap mMap;
    Station station;
    Double latitud;
    Double longitud;
    String name;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_maps);
        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);
        /** Aqui es donde llega la informacion que viene del MainActivity y con el cual viene con
         *  una llave para saber que informacion va a pintar **/
            Intent intent = getIntent();
            if(intent.getExtras() !=null) {
                station = (Station) intent.getSerializableExtra("data");

                 latitud = station.getLatitude();
                 longitud = station.getLongitude();
                 name = station.getName();

            }
    }

    /**
     * Manipulates the map once available.
     * This callback is triggered when the map is ready to be used.
     * This is where we can add markers or lines, add listeners or move the camera. In this case,
     * we just add a marker near Sydney, Australia.
     * If Google Play services is not installed on the device, the user will be prompted to install
     * it inside the SupportMapFragment. This method will only be triggered once the user has
     * installed Google Play services and returned to the app.
     */
    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;

        // Add a marker in Sydney and move the camera
        LatLng sydney = new LatLng(latitud, latitud);
        mMap.addMarker(new MarkerOptions().position(sydney).title(name));
        mMap.moveCamera(CameraUpdateFactory.newLatLng(sydney));

    }

    /** En este metodo es en donde se da la accion al momento de dar un tap ó click en el marcador
     * del mapa**/
    @Override
    public boolean onMarkerClick(Marker marker) {
        String mapRequest = "https://waze.com/ul?q=" + latitud + "," + longitud+
                "&navigate=yes&zoom=17";
        Uri gmmIntentUri = Uri.parse(mapRequest);
        Intent mapIntent = new Intent(Intent.ACTION_VIEW, gmmIntentUri);
        mapIntent.setPackage("com.waze");
        startActivity(mapIntent);
        return true;
    }
}
package giovanni.app.examenendcom.controller.ui.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;


import java.util.List;

import giovanni.app.examenendcom.R;
import giovanni.services.interfaces.Network;
import giovanni.services.interfaces.PuntosResponse;
import giovanni.services.interfaces.Station;

public class EstationAdapter extends RecyclerView.Adapter<EstationAdapter.EstationAdapterVH> {

    private List<Station> stationsResponse;
    private Context context;
    private ClickedItem clickedItem;


    public EstationAdapter(ClickedItem clickedItem) {
        this.clickedItem = clickedItem;

    }
    /** En este metodo es donde se llena la lista con la informacion del response**/
    public void setData(List<Station> stationsResponse){
        this.stationsResponse = stationsResponse;
        notifyDataSetChanged();
    }
    /** En este metodo es donde se declara el xml del cardView**/
    @NonNull
    @Override
    public EstationAdapterVH onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        context = parent.getContext();
        return new EstationAdapter.EstationAdapterVH(LayoutInflater.from(context).inflate(R.layout.row_station_biker,parent,false));
    }
    /** En este metodo es donde se llena los objetos declarados con
     *  la informacion traida del response**/
    @Override
    public void onBindViewHolder(@NonNull EstationAdapterVH holder, int position) {
        Station stationResponse = stationsResponse.get(position);
        String estacion = stationResponse.getName();
        Integer disponibles = stationResponse.getFreeBikes();
        String actualizacion = stationResponse.getTimestamp();

        holder.carEstation.setText(estacion);
        holder.cardDisponible.setText(disponibles);
        holder.cardActualzacion.setText(actualizacion);

    }
    /** Metodo el cual es para dar click en el item seleccionado para hacer un itent al otro Activity**/
    public interface  ClickedItem{
        void ClickedUser(Network stationResponse);
    }

    /** Retorna el numero de item que trae el response**/
    @Override
    public int getItemCount() {
        return stationsResponse.size();
    }

    /** en este metodo ectiende del RecyclerView es el viewHolder dode se declara los objetos del
     * XML el cual declaraste en el metodo  EstationAdapterVH onCreateViewHolder**/
    public class EstationAdapterVH extends RecyclerView.ViewHolder {

        TextView carEstation, cardDisponible, cardActualzacion;

        public EstationAdapterVH(@NonNull View itemView) {
            super(itemView);

            carEstation = itemView.findViewById(R.id.textNameEstation);
            cardDisponible = itemView.findViewById(R.id.textDisponibles);
            cardActualzacion = itemView.findViewById(R.id.textActualizacion);
        }
    }
}

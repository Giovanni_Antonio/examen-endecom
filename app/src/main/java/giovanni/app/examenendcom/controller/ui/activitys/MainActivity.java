package giovanni.app.examenendcom.controller.ui.activitys;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;

import com.mikelau.views.shimmer.ShimmerRecyclerViewX;

import java.util.List;

import giovanni.app.examenendcom.R;
import giovanni.app.examenendcom.controller.ui.adapter.EstationAdapter;
import giovanni.services.apis.ApiClient;
import giovanni.services.interfaces.Network;
import giovanni.services.interfaces.PuntosResponse;
import giovanni.services.interfaces.Station;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MainActivity extends AppCompatActivity implements EstationAdapter.ClickedItem {
    EstationAdapter estationAdapter;
    private ShimmerRecyclerViewX shimmerRecycler;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        shimmerRecycler = findViewById(R.id.shimmer_recycler_view);

        shimmerRecycler.setLayoutManager(new LinearLayoutManager(this));

        shimmerRecycler.showShimmerAdapter();
        getConsumoServices();
    }

    /** En este metodo es donde se llama al servico con el cual se esta ocupando Retrofit ya que es
     * mas sencillo que ocupar ya que solamente es crear una apiclient y
     * los pojos que son get & seter **/
    private void getConsumoServices() {
        Call<Network> userlist = ApiClient.getSercives().getResponses();

        userlist.enqueue(new Callback<Network>() {
            /** En este metodo es donde se llama el metodo response es ocupado cuando el servicio
             * contesta con un 200 junto con el if que valida si es response es corecto entonces
             * deja que corra las otras linea la cual esta encargada en ver en el cuerpo del
             * servicio e ir al jsonArray indicado y el cual va a llenar una lista que se va a
             * mostrar toda la informacion en un recyclerView el cual esta customisado para no
             * poner el clasico load nativo que al momento de estar traiendo informacion se activa
             * la animacion y cuado ya dejo de traen la informacion hacia la lista quita la
             * animacion y muestra la lista con los datos seleccionados en el adapter**/
            @Override
            public void onResponse(Call<Network> call, Response<Network> response) {
                if(response.isSuccessful()){
                    Network network = response.body();
                    List<Station> stations = network.getStations();
                    estationAdapter.setData(stations);
                    shimmerRecycler.setAdapter(estationAdapter);
                    shimmerRecycler.hideShimmerAdapter();
                }
            }

            /** En este metodo es donde se llama un Log por si el servicio contesta con un codigo
             * de error y esto hace que no crashe la aplicacion**/
            @Override
            public void onFailure(Call<Network> call, Throwable t) {
                Log.e("failure",t.getLocalizedMessage());

            }
        });
    }

    /** En este metodo es donde se usa el metodo onclick declarado en el Adapter el cual va a llamar
     *  a la actividad MapsActivity y va a llevar la informacion a mostrar en dicha actividad por
     *  mediante el putExtra**/
    @Override
    public void ClickedUser(Network stationResponse) {
        startActivity(new Intent(this, MapsActivity.class).putExtra("data", String.valueOf(stationResponse)));

    }
}